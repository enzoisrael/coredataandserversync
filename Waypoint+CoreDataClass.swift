//
//  Waypoint+CoreDataClass.swift
//  NetWorkingCoreData
//
//  Created by Enzo Neyra on 4/8/17.
//  Copyright © 2017 Enzo Neyra. All rights reserved.
//

import Foundation
import CoreData

@objc(Waypoint)
public class Waypoint: NSManagedObject {
  
  convenience init(context: NSManagedObjectContext) {
    let entityDescription = NSEntityDescription.entity(forEntityName: "Waypoint", in: context)
    self.init(entity: entityDescription!, insertInto: context)
  }
  
  convenience init(context: NSManagedObjectContext, jsonWaypoint: JSONWaypoint ) {
    let entityDescription = NSEntityDescription.entity(forEntityName: "Waypoint", in: context)
    self.init(entity: entityDescription!, insertInto: context)
    
    name = jsonWaypoint.name
    longitude = jsonWaypoint.longitude
    latitude = jsonWaypoint.latitude
    
  }

}
