//
//  Waypoint+CoreDataProperties.swift
//  NetWorkingCoreData
//
//  Created by Enzo Neyra on 4/8/17.
//  Copyright © 2017 Enzo Neyra. All rights reserved.
//

import Foundation
import CoreData


extension Waypoint {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Waypoint> {
        return NSFetchRequest<Waypoint>(entityName: "Waypoint")
    }

    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var name: String?
    @NSManaged public var trip: Trip?

}
