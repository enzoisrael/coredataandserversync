//
//  ParseJSON.swift
//  Qempo
//
//  Created by Enzo Neyra on 11/02/17.
//  Copyright © 2017 enzoisrael. All rights reserved.
//

import Foundation

// MARK: Protocolo de Json to Struct

public protocol JSONParsealable {
  static func with(json: [String: Any]) -> Self?
}

// MARK: Protocolos de Struct to Json

public protocol JSONRepresentable {
  var JSONRepresentation: AnyObject { get }
}

public protocol JSONSerializable: JSONRepresentable {
  
}

// JSON to Struct

func flatten<A>(x: A??) -> A?{
  if let y = x { return y }
  return nil
}

infix operator >>>=
@discardableResult
public func >>>= <A, B>(optional: A?, f:(A) -> B?) -> B? {
  return flatten(x: optional.map(f))
}

extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any {
  
  public func to_number(key: Key) -> NSNumber? {
    return self[key] >>>= { $0 as? NSNumber }
  }
  
  public func to_int(key: Key) -> Int? {
    return self.to_number(key: key).map { $0.intValue }
  }
  
  public func to_float(key: Key) -> Float? {
    return self.to_number(key: key).map { $0.floatValue }
  }
  
  public func to_double(key: Key) -> Double? {
    return self.to_number(key: key).map { $0.doubleValue }
  }
  
  public func to_string(key: Key) -> String? {
    return self[key] >>>= { $0 as? String }
  }
  
  public func to_bool(key: Key) -> Bool? {
    return self.to_number(key: key).map { $0.boolValue }
  }
  
}

// Struct to JSON

extension JSONSerializable {
  var JSONRepresentation: Any {
    var representation = [String: Any]()
    
    for case let (label?, value) in Mirror(reflecting: self).children {
      
      switch value {
        
      case let value as Dictionary<String, Any>:
        representation[label] = value as AnyObject
        
      case let value as Array<Any>:
        if let val = value as? [JSONSerializable] {
          representation[label] = val.map({ $0.JSONRepresentation as AnyObject }) as AnyObject
        } else {
          representation[label] = value as AnyObject
        }
        
      case let value:
        representation[label] = value as AnyObject
      }
    }
    return representation as AnyObject
  }
}

extension JSONSerializable {
  func toJSON() -> String? {
    let representation = JSONRepresentation
    
    guard JSONSerialization.isValidJSONObject(representation) else {
      print("Invalid JSON Representation")
      return nil
    }
    
    do{
      let data = try JSONSerialization.data(withJSONObject: representation, options: [])
      return String(data: data, encoding: .utf8)
    } catch {
      return nil
    }
  }
}

extension Date: JSONRepresentable {
  public var JSONRepresentation: AnyObject {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    
    return formatter.string(from: self) as AnyObject
  }
  
  
}
















