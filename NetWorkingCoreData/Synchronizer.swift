//
//  Synchronizer.swift
//  NetWorkingCoreData
//
//  Created by Enzo Neyra on 4/8/17.
//  Copyright © 2017 Enzo Neyra. All rights reserved.
//

import Foundation
import CoreData

typealias SynchronizerCallback = ([Trip]) -> Void

class Synchronizer {
  
  var managedObjectContext: NSManagedObjectContext
  
  init(managedObjectContext: NSManagedObjectContext) {
    self.managedObjectContext = managedObjectContext
  }
  
  func sync(callback: @escaping SynchronizerCallback) {
    
    let coreDataClient = CoreDataClient(managedObjectContext: self.managedObjectContext)
    
    APIClient().fetchTrips { trips in
      let coreDataTripsIds = coreDataClient.allTrips().map { $0.serverID! }
      let newServerTrips = trips?.filter {
        !coreDataTripsIds.contains($0.serverID)
      }
      
      newServerTrips?.forEach {
        let trip = Trip(context: self.managedObjectContext, jsonTrip: $0)
        
        $0.waypoints.forEach {
          let waypoint = Waypoint(context: self.managedObjectContext, jsonWaypoint: $0)
          waypoint.trip = trip
        }
      }
      
      try! self.managedObjectContext.save()
      let trips = coreDataClient.allTrips()
      callback(trips)
    }
  }
  
}
