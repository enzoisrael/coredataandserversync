//
//  JSONWaypoint.swift
//
//
//  Created by Enzo Neyra on 4/7/17.
//
//

import Foundation

struct JSONWaypoint {
  let latitude: Double
  let longitude: Double
  let name: String
}

extension JSONWaypoint: JSONParsealable {
  static func with(json: [String: Any]) -> JSONWaypoint? {
    guard
      let latitude = json.to_double(key: "latitude"),
      let longitude = json.to_double(key: "longitude"),
      let name = json.to_string(key: "name")
      else {
        return nil
    }
    return JSONWaypoint(latitude: latitude, longitude: longitude, name: name)
  }
}
