//
//  JSONTrip.swift
//  NetWorkingCoreData
//
//  Created by Enzo Neyra on 4/7/17.
//  Copyright © 2017 Enzo Neyra. All rights reserved.
//

import Foundation

struct JSONTrip {
  let tripName: String
  let waypoints: [JSONWaypoint]
  let serverID: String
}

extension JSONTrip: JSONParsealable {
  static func with(json: [String: Any]) -> JSONTrip? {
    guard
      let tripName = json.to_string(key: "Trip"),
      let serverID = json.to_string(key: "serverID")
      else {
        return nil
    }
    
    let waypointsDicts = json["waypoints"] as? [[String: Any]]
    
    func sanitizedWaypoints(dicts: [[String: Any]]?) -> [JSONWaypoint] {
      guard let dicts = dicts else {
        return [JSONWaypoint]()
      }
      return dicts.flatMap { JSONWaypoint.with(json: $0) }
    }
    
    return JSONTrip(tripName: tripName, waypoints: sanitizedWaypoints(dicts: waypointsDicts), serverID: serverID)
    
  }
}
