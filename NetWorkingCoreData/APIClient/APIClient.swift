//
//  APIClient.swift
//  NetWorkingCoreData
//
//  Created by Enzo Neyra on 4/7/17.
//  Copyright © 2017 Enzo Neyra. All rights reserved.
//

import Foundation

typealias FetchTripsCallback = ([JSONTrip]?) -> Void

class APIClient {
  func fetchTrips(completion: @escaping FetchTripsCallback) {
    let session = URLSession.shared
    let url = URL(string: "http://demo8914872.mockable.io/mstrips")!
    let urlRequest = URLRequest(url: url)
    let task = session.dataTask(with: urlRequest) { (data, response, error) in
      if let data = data {
        let jsonArray = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [[String:Any]]
        
        if let jsonArray = jsonArray {
          var trips = [JSONTrip]()
          DispatchQueue.main.async {
            for json in jsonArray {
              trips.append(JSONTrip.with(json: json)!)
            }
            completion(trips)
          }
        } else {
          DispatchQueue.main.async {
            completion(nil)
          }
        }
      } else {
        DispatchQueue.main.async {
          completion(nil)
        }
      }
    }
    
    task.resume()
  }
}
