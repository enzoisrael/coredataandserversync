//
//  Trip+CoreDataClass.swift
//  NetWorkingCoreData
//
//  Created by Enzo Neyra on 4/8/17.
//  Copyright © 2017 Enzo Neyra. All rights reserved.
//

import Foundation
import CoreData

@objc(Trip)
public class Trip: NSManagedObject {
  
  convenience init(context: NSManagedObjectContext) {
    let entityDescription = NSEntityDescription.entity(forEntityName: "Trip", in: context)
    self.init(entity: entityDescription!, insertInto: context)
  }
  
  convenience init(context: NSManagedObjectContext, jsonTrip: JSONTrip ) {
    let entityDescription = NSEntityDescription.entity(forEntityName: "Trip", in: context)
    self.init(entity: entityDescription!, insertInto: context)
    
    tripName = jsonTrip.tripName
    serverID = jsonTrip.serverID
  }
  
}
