//
//  CoreDataClient.swift
//  NetWorkingCoreData
//
//  Created by Enzo Neyra on 4/8/17.
//  Copyright © 2017 Enzo Neyra. All rights reserved.
//

import Foundation
import CoreData

class CoreDataClient {
  
  var managedObjectContext: NSManagedObjectContext
  
  init(managedObjectContext: NSManagedObjectContext) {
    self.managedObjectContext = managedObjectContext
  }
  
  func allTrips() -> [Trip] {
    let fetchResquest: NSFetchRequest<Trip> = Trip.fetchRequest()
    let trips = try! managedObjectContext.fetch(fetchResquest)
    return trips
  }
  
  
}





