//
//  ViewController.swift
//  NetWorkingCoreData
//
//  Created by Enzo Neyra on 4/7/17.
//  Copyright © 2017 Enzo Neyra. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  var trips = [Trip]()
  var coreDataStack = CoreDataStack(stackType: .SQLite)

  @IBOutlet weak var tableView: UITableView!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    tableView.dataSource = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    trips = CoreDataClient(managedObjectContext: coreDataStack.managedObjectContext).allTrips()
    self.tableView.reloadData()
  
  }
  
  @IBAction func syncButtonTapped(_ sender: Any) {
    Synchronizer(managedObjectContext: coreDataStack.managedObjectContext).sync { (results) in
      self.trips = results
      self.tableView.reloadData()
    }
  }
  

}

extension ViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return trips.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "TripCell")!
    
    cell.textLabel?.text = trips[indexPath.row].tripName
    return cell
  }
  
}

