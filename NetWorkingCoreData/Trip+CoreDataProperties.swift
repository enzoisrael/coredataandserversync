//
//  Trip+CoreDataProperties.swift
//  NetWorkingCoreData
//
//  Created by Enzo Neyra on 4/8/17.
//  Copyright © 2017 Enzo Neyra. All rights reserved.
//

import Foundation
import CoreData


extension Trip {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Trip> {
        return NSFetchRequest<Trip>(entityName: "Trip")
    }

    @NSManaged public var tripName: String?
    @NSManaged public var serverID: String?
    @NSManaged public var waypoints: NSSet?

}

// MARK: Generated accessors for waypoints
extension Trip {

    @objc(addWaypointsObject:)
    @NSManaged public func addToWaypoints(_ value: Waypoint)

    @objc(removeWaypointsObject:)
    @NSManaged public func removeFromWaypoints(_ value: Waypoint)

    @objc(addWaypoints:)
    @NSManaged public func addToWaypoints(_ values: NSSet)

    @objc(removeWaypoints:)
    @NSManaged public func removeFromWaypoints(_ values: NSSet)

}
